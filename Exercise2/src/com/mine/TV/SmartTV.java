package com.mine.TV;

public class SmartTV extends ColoredTV {
	
	private NetworkConnection network;
	
	public SmartTV(String manufacturerName, String modelName) {
		super(manufacturerName, modelName);
	}
	
	public void connectToNetwork (String name) {
		network = new NetworkConnectionImpl(name);
	}
	
	public boolean hasNetworkConnection() {
		if(network == null) {
			return false;
		}
		return network.connectionStatus();
	}
	
	public String getNetworkName() {
		return NetworkConnectionImpl.networkName;
	}
}
