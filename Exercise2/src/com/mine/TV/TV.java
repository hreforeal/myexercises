package com.mine.TV;

public class TV {
	
	private String manufacturerName;
	private String modelName;
	
	private boolean powerOn;
	private int channel;
	private int volume;
	
	public TV(String manufacturerName, String modelName) {
		this.manufacturerName = manufacturerName;
		this.modelName = modelName;
		
		powerOn = false;
		channel = 0;
		volume = 5;
	}
	
	public void turnOn() {
		powerOn = true;
	}
	
	public void turnOff() {
		powerOn = false;
	}
	
	public void channelUp() {
		channel++;
		if(channel > 13) {
			channel = 13;
		}
	}
	
	public void channelDown() {
		channel--;
		if(channel < 0) {
			channel = 0;
		}
	}
	
	public void volumeUp() {
		volume++;
		if(channel > 10) {
			channel = 10;
		}
	}
	
	public void volumeDown() {
		volume--;
		if(volume < 0) {
			volume = 0;
		}
	}
	
	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	public void setChannel(int channel) {
		this.channel = channel;
	}
	
	public String toString() {
		return "[ Manufacturer: " + manufacturerName + "; Model: " 
			   + modelName + "] \t [ On: " + powerOn + "; Channel: " 
			   + channel +  "; Volume: " + volume + " ] \t";
	}
}
