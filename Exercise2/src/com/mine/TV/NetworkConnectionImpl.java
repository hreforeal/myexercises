package com.mine.TV;

public class NetworkConnectionImpl implements NetworkConnection {

	public static String networkName;
	private boolean isConnected;
	
	public NetworkConnectionImpl(String name) {
		networkName = name;
		isConnected = true;
	}
	
	public NetworkConnection connect(String name) {
		return new NetworkConnectionImpl(name);
	}
	
	public boolean connectionStatus() {
		return isConnected;
	}
}
