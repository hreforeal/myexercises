package com.mine.TV;

public class MainClass {
	
	public static void main(String[] args) {
/*Generic TV Test Script*/
//		TV myTV = new TV("Andre Electronics", "ONE");
//		System.out.println(myTV);
//		
//		myTV.turnOn();
//
//		for(int i = 0; i < 5; i ++) {
//			myTV.channelUp();
//		}
//		myTV.channelDown();
//		
//		for(int i = 0; i < 3; i ++) {
//			myTV.volumeDown();
//		}
//		myTV.volumeUp();
//
//		myTV.turnOff();
//		
//		System.out.println(myTV);
		
/*Colored TV Test Script*/
//		TV bnwTV;
//		ColoredTV sonyTV;
//		
//		bnwTV = new TV("Admiral", "A1");
//		sonyTV = new ColoredTV("Sony", "S1");
//		
//		System.out.println(bnwTV);
//		System.out.println(sonyTV);
//		
//		sonyTV.brightnessUp();
//		
//		System.out.println(sonyTV);
//		
//		ColoredTV sharpTV = new ColoredTV("Sharp", "SH1");
//		sharpTV.mute();
//		
//		for(int i = 0; i < 10; i++) {
//			sharpTV.brightnessUp();
//		}
//		
//		sharpTV.brightnessDown();
//		
//		for(int i = 0; i < 15; i++) {
//			sharpTV.contrastDown();
//		}
//		
//		sharpTV.contrastUp();
//		
//		for(int i = 0; i < 20; i++) {
//			sharpTV.pictureUp();
//		}
//		
//		for(int i = 0; i < 5; i++) {
//			sharpTV.pictureDown();
//		}
//		
//		System.out.println(sharpTV);
//		
//		for(int i = 0; i < 5; i ++) {
//			sharpTV.channelUp();
//		}
//		
//		sharpTV.setChannel(2);
//		
//		for(int i = 0; i < 3; i ++) {
//			sharpTV.volumeUp();
//		}
//		
//		System.out.println(sharpTV);
//		
//		sharpTV.mute();
//		
//		System.out.println(sharpTV);	
		
/*Smart TV Test Script*/
//		SmartTV samsungTV = new SmartTV("Samsung", "SMTV1");
//		System.out.println(samsungTV);
//
//		samsungTV.mute();
//
//		for(int i = 0; i < 3; i ++) {
//			samsungTV.brightnessUp();
//		}
//		
//		for(int i = 0; i < 5; i ++) {
//			samsungTV.brightnessDown();
//		}
//		
//		for(int i = 0; i < 3; i ++) {
//			samsungTV.contrastUp();
//		}
//		
//		for(int i = 0; i < 5; i ++) {
//			samsungTV.contrastDown();
//		}
//		
//		for(int i = 0; i < 3; i ++) {
//			samsungTV.pictureUp();
//		}
//		
//		samsungTV.pictureDown();
//
//		samsungTV.channelUp();
//		samsungTV.channelUp();
//		
//		System.out.println(samsungTV);		
//		
//		samsungTV.connectToNetwork("TestNetwork");
//		System.out.println(samsungTV.getNetworkName());
		
	}
}

