package com.mine.TV;

public class ColoredTV extends TV {

	private int brightness;
	private int contrast;
	private int picture;
	
	public ColoredTV(String manufacturerName, String modelName) {
		super(manufacturerName, modelName);
		
		brightness = 50;
		contrast = 50;
		picture = 50;
	}
	
	public void brightnessUp() {
		brightness++;
	}
	
	public void brightnessDown() {
		brightness--;
	}
	
	public void contrastUp() {
		contrast++;
	}
	
	public void contrastDown() {
		contrast--;
	}
	
	public void pictureUp() {
		picture++;
	}
	
	public void pictureDown() {
		picture--;
	}
	
	public void swtichToChannel(int channel) {
		super.setChannel(channel);
	}
	
	public void mute() {
		super.setVolume(0);
	}
	
	public String toString()
	{
		return super.toString() 
			   + " [ b: " + brightness + "; c: " 
			   + contrast + "; p: " + picture + " ]";
	}
	
}
