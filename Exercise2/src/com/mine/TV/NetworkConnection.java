package com.mine.TV;

public interface NetworkConnection {
	
	public NetworkConnection connect(String name);
	public boolean connectionStatus();
	
}
