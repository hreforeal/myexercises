package com.elavon.training.main;

import com.elavon.training.impl.MyImplementation;

public class Main {
    public static void main(final String[] args) {
        final MyImplementation myImpl = new MyImplementation();

        System.out.println("1 + 1 = " + myImpl.getSum(1, 1));
        System.out.println("2 + 3 = " + myImpl.getSum(2, 3));
        System.out.println("5 + 8 = " + myImpl.getSum(5, 8));
        System.out.println("13 + 21 = " + myImpl.getSum(13, 21));
        System.out.println("34 + 55 = " + myImpl.getSum(34, 55));

        System.out.println("1 - 1 = " + myImpl.getDifference(1, 1));
        System.out.println("55.75 - 23.4 = " + myImpl.getDifference(55.75, 23.4));
        System.out.println("8.6 - 5.1 = " + myImpl.getDifference(8.6, 5.1));
        System.out.println("21 - 13 = " + myImpl.getDifference(21, 13));
        System.out.println("55 - 34 = " + myImpl.getDifference(55, 34));

        System.out.println("5 x 10 = " + myImpl.getProduct(5, 10));
        System.out.println("-1 x 24 = " + myImpl.getProduct(-1, 24));
        System.out.println("12 x 12 = " + myImpl.getProduct(12, 12));
        System.out.println("-5 x -5 = " + myImpl.getProduct(-5, -5));
        System.out.println("6 x 5 = " + myImpl.getProduct(6, 5));

        System.out.println("10 / 5 = " + myImpl.getQuotientAndRemainder(10, 5));
        System.out.println("10 / 3 = " + myImpl.getQuotientAndRemainder(-1, 24));
        System.out.println("0 / 3 = " + myImpl.getQuotientAndRemainder(0, 3));
        System.out.println("144 / 13 = " + myImpl.getQuotientAndRemainder(144, 13));
        System.out.println("21 / 6 = " + myImpl.getQuotientAndRemainder(21, 6));

        System.out.println("37F is %.2f" + myImpl.toCelsius(37) + "C");
        System.out.println("45F is " + myImpl.toCelsius(45) + "C");
        System.out.println("-37F is " + myImpl.toCelsius(-37) + "C");

        System.out.println("100C is " + myImpl.toFahrenheit(100) + "F");
        System.out.println("-37C is " + myImpl.toFahrenheit(-37) + "F");
        System.out.println("35C is " + myImpl.toFahrenheit(35) + "F");

        System.out.println("25kg is " + myImpl.toPound(25) + "lbs");
        System.out.println("100kg is " + myImpl.toPound(100) + "lbs");
        System.out.println("65kg is " + myImpl.toPound(65) + "lbs");

        System.out.println("200lbs is " + myImpl.toKilogram(200) + "kg");
        System.out.println("120lbs is " + myImpl.toKilogram(200) + "kg");
        System.out.println("65lbs is " + myImpl.toKilogram(200) + "kg");

        System.out.println("level: " + myImpl.isPalindrome("Racecar"));
        System.out.println("Person: " + myImpl.isPalindrome("Person"));
        System.out.println("TatTaRraTtAt: " + myImpl.isPalindrome("TatTaRraTtAt"));
        System.out.println("RotAvatOr: " + myImpl.isPalindrome("RotAvatOr"));
        System.out.println("Hello: " + myImpl.isPalindrome("Hello"));
    }

}
