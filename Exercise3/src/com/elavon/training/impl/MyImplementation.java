package com.elavon.training.impl;

import com.elavon.training.myinterface.AwesomeCalculator;

public class MyImplementation implements AwesomeCalculator {

    private final double TOCELSIUS = 0.56;
    private final double TOFARENHEIT = 1.8;
    private final int TEMPCONSTANT = 32;
    
    private final double WEIGHTCONSTANT = 2.2046;
    
    @Override
    public int getSum(final int augend, final int addend) {
        return augend + addend;
    }

    @Override
    public double getDifference(final double minuend, final double subtrahend) {
        return minuend - subtrahend;
    }

    @Override
    public double getProduct(final double multiplicand, final double multiplier) {
        return multiplicand * multiplier;
    }

    @Override
    public String getQuotientAndRemainder(final int dividend, final int divisor) {
        return (dividend / divisor) + " remainder " + (dividend % divisor);
    }

    @Override
    public double toCelsius(final int fahrenheit) {
        return TOCELSIUS * (fahrenheit - TEMPCONSTANT);
    }

    @Override
    public double toFahrenheit(final int celsius) {
        return (TOFARENHEIT * celsius) + TEMPCONSTANT;
    }

    @Override
    public double toKilogram(final double lbs) {
        return lbs / WEIGHTCONSTANT;
    }

    @Override
    public double toPound(final double kg) {
        return kg * WEIGHTCONSTANT;
    }

    @Override
    public boolean isPalindrome(final String str) {

        if (str.length() <= 1) {
            return true;
        }

        if (Character.toLowerCase(str.charAt(0)) == Character.toLowerCase(str.charAt(str.length() - 1))) {
            return isPalindrome(str.substring(1, str.length() - 1));
        }

        return false;
    }

}
